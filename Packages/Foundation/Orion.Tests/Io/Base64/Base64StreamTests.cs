﻿using NUnit.Framework;
using System;
using System.IO;
using System.Text;

namespace Orion.Io.Base64
{
    [TestFixture]
    public class Base64StreamTests
    {
        #region Helpers

        static string Encode(byte[] data)
        {
            byte[] encodedData = null;

            using (MemoryStream stream = new MemoryStream())
            using (Base64Stream writer = new Base64Stream(stream, Base64StreamMode.Encode))
            {
                //foreach (byte b in data)
                //    writer.WriteByte(b);
                writer.Write(data, 0, data.Length);
                writer.Flush();

                encodedData = stream.ToArray();
            }

            return Encoding.Default.GetString(encodedData);
        }

        static byte[] Decode(string data)
        {
            byte[] dataToDecode = Encoding.Default.GetBytes(data);
            byte[] decodedData = new byte[BaseAlgorithmCoder.CalcDecodedLength(data)];

            using (MemoryStream stream = new MemoryStream(dataToDecode))
            using (Base64Stream reader = new Base64Stream(stream, Base64StreamMode.Decode))
            {
                int c = 0;
                int i = 0;
                while ((c = reader.ReadByte()) >= 0)
                    decodedData[i++] = (byte)c;
            }

            return decodedData;
        }

        #endregion


        [Test]
        [TestCaseSource(typeof(TestDataSet), nameof(TestDataSet.StringsToDecode))]
        public void Decode_Base64String_StringMessage(string source)
        {
            Assert.That(source.Length % 4, Is.Zero, "Invalid length of source string.");

            byte[] dataToDecode = Encoding.Default.GetBytes(source);

            var streamDecoded = Encoding.Default.GetString(Decode(source));
            var convertDecoded = Encoding.Default.GetString(Convert.FromBase64String(source));
            var algorithmDecoded = Encoding.Default.GetString(BaseAlgorithmCoder.Decode(source));

            TestContext.Out.WriteLine($"Message: '{source}'; Length = {source.Length}");
            TestContext.Out.WriteLine($"Base64Stream: '{streamDecoded}'; Length = {streamDecoded.Length}");
            TestContext.Out.WriteLine($".NET Convert: '{convertDecoded}'; Length = {convertDecoded.Length}");
            TestContext.Out.WriteLine($"Algorithm: '{algorithmDecoded}'; Length = {algorithmDecoded.Length}");

            Assert.That(streamDecoded, Is.EqualTo(convertDecoded), "Base64Stream returns an invalid source string.");
            Assert.That(algorithmDecoded, Is.EqualTo(convertDecoded), "Algorithm returns an invalid source string.");
        }

        [Test]
        [TestCaseSource(typeof(TestDataSet), nameof(TestDataSet.BinarySequencesToDecode))]
        public void Decode_Base64String_BinaryData(string source)
        {
            Assert.That(source.Length % 4, Is.Zero, "Invalid length of source string.");

            var streamDecoded = Decode(source);
            var convertDecoded = Convert.FromBase64String(source);
            var algorithmDecoded = BaseAlgorithmCoder.Decode(source);

            TestContext.Out.WriteLine($"Source: '{source}'; Length = {source.Length}");
            TestContext.Out.WriteLine($"Base64Stream: Length = {streamDecoded.Length}");
            TestContext.Out.WriteLine($".NET Convert: Length = {convertDecoded.Length}");
            TestContext.Out.WriteLine($"Algorithm: Length = {algorithmDecoded.Length}");

            Assert.That(streamDecoded, Is.EquivalentTo(convertDecoded), "Base64Stream returns an invalid decoded data.");
            Assert.That(algorithmDecoded, Is.EquivalentTo(convertDecoded), "Algorithm returns an invalid decoded string.");
        }
    }
}
