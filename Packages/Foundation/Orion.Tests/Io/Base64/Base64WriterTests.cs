﻿using NUnit.Framework;
using System;
using System.IO;
using System.Text;

namespace Orion.Io.Base64
{
    [TestFixture]
    public class Base64WriterTests
    {
        #region Helpers

        public static string Encode(byte[] data)
        {
            byte[] encodedData = null;

            using (MemoryStream stream = new MemoryStream())
            using (Base64Writer writer = new Base64Writer(stream))
            {
                //foreach (byte b in data)
                //    writer.Write(b);
                writer.Write(data, 0, data.Length);
                writer.Flush();

                encodedData = stream.ToArray();
            }

            return Encoding.Default.GetString(encodedData);
        }

        #endregion


        [Test]
        [TestCaseSource(typeof(TestDataSet), nameof(TestDataSet.StringsToEncode))]
        public void Encode_StringMessage_Base64String(string message)
        {
            byte[] dataToEncode = Encoding.Default.GetBytes(message);

            var streamEncoded = Encode(dataToEncode);
            var convertEncoded = Convert.ToBase64String(dataToEncode);
            var algorithmEncoded = BaseAlgorithmCoder.Encode(dataToEncode);

            TestContext.Out.WriteLine($"Message: '{message}'; Length = {message.Length}");
            TestContext.Out.WriteLine($"Base64Writer: '{streamEncoded}'; Length = {streamEncoded.Length}");
            TestContext.Out.WriteLine($".NET Convert: '{convertEncoded}'; Length = {convertEncoded.Length}");
            TestContext.Out.WriteLine($"Algorithm: '{algorithmEncoded}'; Length = {algorithmEncoded.Length}");

            Assert.That(streamEncoded.Length % 4, Is.Zero, "Base64Writer returns a code-string with invalid length.");
            Assert.That(streamEncoded, Is.EqualTo(convertEncoded), "Base64Writer returns an invalid code-string.");
            Assert.That(algorithmEncoded.Length % 4, Is.Zero, "Algorithm returns a code-string with invalid length.");
            Assert.That(algorithmEncoded, Is.EqualTo(convertEncoded), "Algorithm returns an invalid code-string.");
        }

        [Test]
        [TestCaseSource(typeof(TestDataSet), nameof(TestDataSet.BinarySequencesToEncode))]
        public void Encode_BinaryData_Base64String(byte[] source)
        {
            var streamEncoded = Encode(source);
            var convertEncoded = Convert.ToBase64String(source);
            var algorithmEncoded = BaseAlgorithmCoder.Encode(source);

            TestContext.Out.WriteLine($"Source: Length = {source.Length}");
            TestContext.Out.WriteLine($"Base64Writer: '{streamEncoded}'; Length = {streamEncoded.Length}");
            TestContext.Out.WriteLine($".NET Convert: '{convertEncoded}'; Length = {convertEncoded.Length}");
            TestContext.Out.WriteLine($"Algorithm: '{algorithmEncoded}'; Length = {algorithmEncoded.Length}");

            Assert.That(streamEncoded.Length % 4, Is.Zero, "Base64Writer returns a code-string with invalid length.");
            Assert.That(streamEncoded, Is.EqualTo(convertEncoded), "Base64Writer returns an invalid code-string.");
            Assert.That(algorithmEncoded.Length % 4, Is.Zero, "Algorithm returns a code-string with invalid length.");
            Assert.That(algorithmEncoded, Is.EqualTo(convertEncoded), "Algorithm returns an invalid code-string.");
        }

        [Test]
        public void Flush_BinaryData_Base64String()
        {
            byte[] source = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

            string convertEncoded = Convert.ToBase64String(source);
            string streamEncoded;

            using (MemoryStream stream = new MemoryStream())
            using (Base64Writer writer = new Base64Writer(stream))
            {
                for (int i = 0; i < source.Length; i++)
                {
                    writer.Write(source, i, 1);
                    writer.Flush();
                }

                streamEncoded = Encoding.UTF8.GetString(stream.ToArray());
            }

            Assert.That(streamEncoded, Is.EqualTo(convertEncoded), "Base64Writer returns an invalid code-string.");
        }
    }
}
