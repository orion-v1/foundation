﻿using NUnit.Framework;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Orion.Io.Base64
{
    public static class TestDataSet
    {
        #region String

        public static IReadOnlyList<string> StringsToEncode => new[]
        {
            "F",
            "Fo",
            "Foo",
            "Foo+",
            "Foo+B",
            "Foo+Ba",
            "Foo+Bar",
            "A message #1 to test base64 encoder",
            "A message #10 to test base64 encoder",
            "A message #100 to test base64 encoder",
        };


        public static IReadOnlyList<string> StringsToDecode => StringsToEncode
            .Select(p => Convert.ToBase64String(Encoding.Default.GetBytes(p)))
            .ToArray();

        #endregion

        #region Binary

        private readonly static IReadOnlyList<BinarySequencePair> BinarySequences = new[]
        {
            new BinarySequencePair("SweepBinarySequence, lenght = 255", GenerateSweepBinarySequence(255)),
            new BinarySequencePair("SweepBinarySequence, lenght = 256", GenerateSweepBinarySequence(265)),
            new BinarySequencePair("SweepBinarySequence, lenght = 257", GenerateSweepBinarySequence(257)),
            new BinarySequencePair("SweepBinarySequence, lenght = 1024", GenerateSweepBinarySequence(1024)),
            new BinarySequencePair("RandomBinarySequence, lenght = 127", GenerateRandomBinarySequence(127)),
            new BinarySequencePair("RandomBinarySequence, lenght = 128", GenerateRandomBinarySequence(128)),
            new BinarySequencePair("RandomBinarySequence, lenght = 129", GenerateRandomBinarySequence(129)),
            new BinarySequencePair("RandomBinarySequence, lenght = 1024", GenerateRandomBinarySequence(1024)),
        };


        public static IEnumerable BinarySequencesToEncode
        {
            get
            {
                foreach (var item in BinarySequences)
                    yield return new TestCaseData(item.SequenceToEncode).SetName(item.Name);
            }
        }

        public static IEnumerable BinarySequencesToDecode
        {
            get
            {
                foreach (var item in BinarySequences)
                    yield return new TestCaseData(item.SequenceToDecode).SetName(item.Name);
            }
        }

        public static byte[] GenerateSweepBinarySequence(int length)
        {
            var data = new byte[length];
            for (int i = 0; i < length; i++)
                data[i] = (byte)(i % 256);

            return data;
        }

        public static byte[] GenerateRandomBinarySequence(int length)
        {
            var rnd = new Random();
            var data = new byte[length];

            for (int i = 0; i < length; i++)
                data[i] = (byte)(rnd.Next(256));

            return data;
        }


        public class BinarySequencePair
        {
            public readonly string Name;
            public readonly byte[] SequenceToEncode;
            public readonly string SequenceToDecode;

            public BinarySequencePair(string name, byte[] sequenceToEncode)
            {
                Name = name;
                SequenceToEncode = sequenceToEncode;
                SequenceToDecode = Convert.ToBase64String(sequenceToEncode);
            }
        }

        #endregion
    }
}
