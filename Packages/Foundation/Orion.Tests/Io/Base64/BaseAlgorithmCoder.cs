﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Orion.Io.Base64
{
    // Source: https://en.wikibooks.org/wiki/Algorithm_Implementation/Miscellaneous/Base64
    internal static class BaseAlgorithmCoder
    {
        private const string encodeLookup = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
        private const char padCharacter = '=';


        public static string Encode(byte[] data)
        {
            int inputBufferDiv = data.Length / 3;
            int inputBufferMod = data.Length % 3;
            int encodedStringCapacity = (inputBufferDiv + inputBufferMod) * 4;
            StringBuilder encodedString = new StringBuilder(encodedStringCapacity);

            int temp;
            int idx = 0;
            int idx_max = inputBufferDiv * 3;
            while (idx < idx_max)
            {
                temp = (data[idx++]) << 16; //Convert to big endian
                temp += (data[idx++]) << 8;
                temp += (data[idx++]);

                encodedString.Append(encodeLookup[(temp & 0x00FC0000) >> 18]);
                encodedString.Append(encodeLookup[(temp & 0x0003F000) >> 12]);
                encodedString.Append(encodeLookup[(temp & 0x00000FC0) >> 6]);
                encodedString.Append(encodeLookup[(temp & 0x0000003F)]);
            }

            switch (inputBufferMod)
            {
                case 1:
                    temp = (data[idx++]) << 16; //Convert to big endian
                    encodedString.Append(encodeLookup[(temp & 0x00FC0000) >> 18]);
                    encodedString.Append(encodeLookup[(temp & 0x0003F000) >> 12]);
                    encodedString.Append(padCharacter);
                    encodedString.Append(padCharacter);
                    break;

                case 2:
                    temp = (data[idx++]) << 16; //Convert to big endian
                    temp += (data[idx++]) << 8;
                    encodedString.Append(encodeLookup[(temp & 0x00FC0000) >> 18]);
                    encodedString.Append(encodeLookup[(temp & 0x0003F000) >> 12]);
                    encodedString.Append(encodeLookup[(temp & 0x00000FC0) >> 6]);
                    encodedString.Append(padCharacter);
                    break;
            }

            return encodedString.ToString();
        }

        public static byte[] Decode(string data)
        {
            if (data.Length % 4 != 0) // Sanity check
                throw new Exception("Non-Valid base64!");

            // Setup a vector to hold the result
            int decodedBytesLength = CalcDecodedLength(data);
            List<byte> decodedBytes = new List<byte>(decodedBytesLength);
            int temp = 0; // Holds decoded quanta
            int idx = 0;
            while (idx < data.Length)
            {
                for (int quantumPosition = 0; quantumPosition < 4; quantumPosition++)
                {
                    temp <<= 6;
                    char cursor = data[idx];
                    if (cursor >= 0x41 && cursor <= 0x5A) // This area will need tweaking if
                        temp |= cursor - 0x41;		              // you are using an alternate alphabet
                    else if (cursor >= 0x61 && cursor <= 0x7A)
                        temp |= cursor - 0x47;
                    else if (cursor >= 0x30 && cursor <= 0x39)
                        temp |= cursor + 0x04;
                    else if (cursor == 0x2B)
                        temp |= 0x3E; // change to 0x2D for URL alphabet
                    else if (cursor == 0x2F)
                        temp |= 0x3F; // change to 0x5F for URL alphabet
                    else if (cursor == padCharacter) //pad
                    {
                        switch (data.Length - idx)
                        {
                            case 1: // One pad character
                                decodedBytes.Add((byte)((temp >> 16) & 0x000000FF));
                                decodedBytes.Add((byte)((temp >> 8) & 0x000000FF));
                                return decodedBytes.ToArray();

                            case 2: // Two pad characters
                                decodedBytes.Add((byte)((temp >> 10) & 0x000000FF));
                                return decodedBytes.ToArray();

                            default:
                                throw new Exception("Invalid Padding in Base 64!");
                        }
                    }
                    else
                        throw new Exception("Non-Valid Character in Base 64!");

                    idx++;
                }

                decodedBytes.Add((byte)((temp >> 16) & 0x000000FF));
                decodedBytes.Add((byte)((temp >> 8) & 0x000000FF));
                decodedBytes.Add((byte)((temp) & 0x000000FF));
            }

            return decodedBytes.ToArray();
        }

        public static int CalcDecodedLength(string data)
        {
            int length = data.Length;
            int padding = 0;

            if (length > 0)
            {
                if (data[length - 1] == padCharacter) padding++;
                if (data[length - 2] == padCharacter) padding++;
            }

            return (length / 4 * 3) - padding;
        }
    }
}
