﻿namespace Orion.Ioc
{
    public interface IService
    {
        int ServiceId { get; }
        int ObjectId { get; }

        bool IsDisposed { get; }
    }
}
