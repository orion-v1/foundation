﻿namespace Orion.Ioc
{
    public class ServiceBDependsA : ServiceBase, IServiceB
    {
        private readonly IServiceA _service;

        public ServiceBDependsA(IServiceA service)
        {
            _service = service;
        }

        public override string ToString()
        {
            return $"{base.ToString()} Service A = {_service?.ToString()}";
        }
    }
}
