﻿using NUnit.Framework;
using System;

namespace Orion.Ioc
{
    public class AmbiguousConstructorService
    {
        public AmbiguousConstructorService(IServiceContainer container, IServiceProvider provider)
        {
            TestContext.Out.WriteLine($"Container: {container}");
            TestContext.Out.WriteLine($"Provider: {provider}");
        }

        public AmbiguousConstructorService(IServiceContainer container, IServiceScopeFactory scopeFactory)
        {
            TestContext.Out.WriteLine($"Container: {container}");
            TestContext.Out.WriteLine($"Scope Factory: {scopeFactory}");
        }
    }
}
