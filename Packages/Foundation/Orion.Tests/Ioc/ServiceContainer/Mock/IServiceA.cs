﻿namespace Orion.Ioc
{
    public interface IServiceA : IService
    {
        void Log();
    }
}
