using NUnit.Framework;
using System;
using System.Threading.Tasks;
using UnityEngine.TestTools;

namespace Orion.Threading
{
    [TestFixture]
    public class ForgetTests
    {
        private static async Task DoRanToCompletionTaskAsync()
        {
            await Task.Delay(200);
        }

        private static async Task DoFaultedTaskAsync()
        {
            await Task.Delay(200);
            throw new Exception();
        }

        private static async Task DoCanceledTaskAsync()
        {
            await Task.Delay(200);
            throw new OperationCanceledException();
        }


        [Test]
        public async Task Forget_RanToCompletionTask()
        {
            var task = DoRanToCompletionTaskAsync();
            task.Forget();

            while (!task.IsCompleted) await Task.Yield();
        }

        [Test]
        public async Task Forget_RanFaultedTask()
        {
            var task = DoFaultedTaskAsync();
            task.Forget();

            while (!task.IsCompleted) await Task.Yield();
        }

        [Test]
        public async Task Forget_CanceledTask()
        {
            var task = DoCanceledTaskAsync();
            task.Forget();

            while (!task.IsCompleted) await Task.Yield();
        }

        [Test]
        public async Task Forget_FaultedTask_LogErrors()
        {
            LogAssert.ignoreFailingMessages = true;
            var task = DoFaultedTaskAsync();
            task.Forget(ForgetOptions.LogFaulted);

            while (!task.IsCompleted) await Task.Yield();

            TestContext.WriteLine("ok");
        }

        [Test]
        public async Task Forget_FaultedTask_NoLogErrors()
        {
            LogAssert.ignoreFailingMessages = true;
            var task = DoFaultedTaskAsync();
            task.Forget(ForgetOptions.None);

            while (!task.IsCompleted) await Task.Yield();

            TestContext.WriteLine("ok");
        }

        [Test]
        public async Task Forget_CanceledTask_LogErrors()
        {
            LogAssert.ignoreFailingMessages = true;
            var task = DoCanceledTaskAsync();
            task.Forget(ForgetOptions.LogCanceled);

            while (!task.IsCompleted) await Task.Yield();

            TestContext.WriteLine("ok");
        }

        [Test]
        public async Task Forget_CanceledTask_NoLogErrors()
        {
            LogAssert.ignoreFailingMessages = true;
            var task = DoCanceledTaskAsync();
            task.Forget(ForgetOptions.None);

            while (!task.IsCompleted) await Task.Yield();

            TestContext.WriteLine("ok");
        }
    }
}
