﻿using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEditor;

namespace Orion.Reflection.Editor
{
    public static class PlayerSettingsUtils
    {
        private static SerializedObject GetSerializedObject()
        {
            var mi = typeof(PlayerSettings).GetMethod("GetSerializedObject", BindingFlags.Static | BindingFlags.NonPublic);
            return mi.Invoke(null, null) as SerializedObject;
        }

        public static IReadOnlyList<BuildTargetGroup> GetBuildTargetGroupsForScriptingDefineSymbols()
        {
            SerializedObject so = GetSerializedObject();
            so.UpdateIfRequiredOrScript();

            var arrProperty = so.FindProperty("scriptingDefineSymbols");

            int size = arrProperty.arraySize;
            var rs = new List<BuildTargetGroup>(size);
            for (int i = 0; i < size; i++)
            {
                var element = arrProperty.GetArrayElementAtIndex(i);
                if (element.type != "pair") throw new FormatException();

                var first = element.FindPropertyRelative("first");
                if (first == null) throw new FormatException();

                BuildTargetGroup group = BuildTargetGroup.Unknown;
                switch (first.type)
                {
                    case "int":
                        group = (BuildTargetGroup)first.intValue;
                        break;

                    case "string": // for Unity 2021+
                        group = (BuildTargetGroup)Enum.Parse(typeof(BuildTargetGroup), first.stringValue, true);
                        break;

                    default:
                        throw new NotImplementedException("Failed to parse PlayerSettings.");
                }

                rs.Add(group);
            }

            return rs;
        }
    }
}
