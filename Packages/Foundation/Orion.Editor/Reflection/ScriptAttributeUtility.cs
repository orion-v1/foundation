﻿using System;
using System.Reflection;
using UnityEditor;

namespace Orion.Reflection.Editor
{
    public static class ScriptAttributeUtility
    {
        private static Type _targetType;

        private delegate FieldInfo GetFieldInfoAndStaticTypeFromProperty(SerializedProperty property, out Type type);
        private static GetFieldInfoAndStaticTypeFromProperty _getFieldInfoAndStaticTypeFromProperty;

        public static Type TargetType
        {
            get
            {
                if (_targetType == null)
                    _targetType = Type.GetType("UnityEditor.ScriptAttributeUtility, UnityEditor")
                        ?? throw new NotImplementedException("Failed to reflect type UnityEditor.ScriptAttributeUtility");
                
                return _targetType;
            }
        }


        public static FieldInfo GetFieldInfoAndStaticType(SerializedProperty property, out Type type)
        {
            if (_getFieldInfoAndStaticTypeFromProperty == null)
            {
                MethodInfo mi = TargetType.GetMethod("GetFieldInfoAndStaticTypeFromProperty", BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
                _getFieldInfoAndStaticTypeFromProperty = (GetFieldInfoAndStaticTypeFromProperty)Delegate.CreateDelegate(typeof(GetFieldInfoAndStaticTypeFromProperty), mi);

                if (_getFieldInfoAndStaticTypeFromProperty == null)
                    throw new NotImplementedException("Failed to reflect method UnityEditor.ScriptAttributeUtility.GetFieldInfoAndStaticTypeFromProperty");
            }

            return _getFieldInfoAndStaticTypeFromProperty(property, out type);
        }

        public static FieldInfo GetFieldInfoAndStaticType(SerializedProperty property)
            => GetFieldInfoAndStaticType(property, out _);

        public static T GetCustomAttributeFromProperty<T>(SerializedProperty property) where T : Attribute
        {
            var info = GetFieldInfoAndStaticType(property);
            return info?.GetCustomAttribute<T>();
        }
    }
}
