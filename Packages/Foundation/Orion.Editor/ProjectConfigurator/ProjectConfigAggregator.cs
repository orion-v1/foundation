﻿using System;
using System.Collections;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using UnityEditor;
using UnityEngine;

namespace Orion.ProjectConfig.Editor
{
    internal class ProjectConfigAggregator : AssetPostprocessor
    {
        public const string ConfigFilename = "orion-config.xml";

        private static void OnPostprocessAllAssets(string[] importedAssets, string[] deletedAssets, string[] movedAssets, string[] movedFromAssetPaths)
        {
            bool needToProcess = false;

            foreach (string assetPath in importedAssets)
                if (assetPath.EndsWith(ConfigFilename))
                {
                    needToProcess = true;
                    break;
                }

            if (!needToProcess)
            {
                foreach (string assetPath in deletedAssets)
                    if (assetPath.EndsWith(ConfigFilename))
                    {
                        needToProcess = true;
                        break;
                    }
            }

            if (needToProcess) Run();
        }

        //[MenuItem("Orion/Reapply project config")]
        [InitializeOnLoadMethod]
        public static void Run()
        {
            if (EditorApplication.isPlayingOrWillChangePlaymode) return;

            IProjectConfigurator[] configurators = new[]
            {
                new PreprocessorDirectivesConfigurator()
            };

            // find config files and parse
            var paths = Directory.EnumerateFiles("Assets", ConfigFilename, SearchOption.AllDirectories);
            if (Directory.Exists("Library/PackageCache"))
                paths = paths.Concat(Directory.EnumerateFiles("Library/PackageCache", ConfigFilename, SearchOption.AllDirectories));

            foreach (var path in paths)
            {
                XDocument doc = XDocument.Load(path);

                foreach (var configurator in configurators)
                    configurator.AddConfig(doc);
            }

            // apply
            foreach (var configurator in configurators)
                configurator.Apply();
        }
    }
}
