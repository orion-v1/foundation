﻿namespace Orion.ProjectConfig.Editor
{
    internal interface IManagedDirectiveComparer
    {
        bool IsMatched(string directive);
    }
}
