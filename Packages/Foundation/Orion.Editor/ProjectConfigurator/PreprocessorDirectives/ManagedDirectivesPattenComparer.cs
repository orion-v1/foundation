﻿using System.Text.RegularExpressions;

namespace Orion.ProjectConfig.Editor
{
    internal class ManagedDirectivesPattenComparer : IManagedDirectiveComparer
    {
        private readonly string _pattern;

        public ManagedDirectivesPattenComparer(string pattern)
        {
            _pattern = pattern;
        }

        public bool IsMatched(string directive) => Regex.IsMatch(directive, _pattern);
    }
}
