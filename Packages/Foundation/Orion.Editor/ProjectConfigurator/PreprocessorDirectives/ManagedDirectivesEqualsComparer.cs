﻿namespace Orion.ProjectConfig.Editor
{
    internal class ManagedDirectivesEqualsComparer : IManagedDirectiveComparer
    {
        private readonly string _directive;

        public ManagedDirectivesEqualsComparer(string directive)
        {
            _directive = directive;
        }

        public bool IsMatched(string directive) => directive == _directive;
    }
}
