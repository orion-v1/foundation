﻿namespace Orion.ProjectConfig.Editor
{
    internal class RequestedDirective
    {
        public string Name { get; }   
        public OrCondition Conditions { get; }

        public RequestedDirective(string name)
        {
            Name = name;
            Conditions = new OrCondition();
        }

        public bool IsMatched() => Conditions.IsMatched();
    }
}
