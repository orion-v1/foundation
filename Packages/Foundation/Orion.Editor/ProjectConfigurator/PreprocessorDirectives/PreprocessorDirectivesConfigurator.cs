﻿using Orion.Editor;
using Orion.Reflection.Editor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using UnityEditor;

namespace Orion.ProjectConfig.Editor
{
    internal class PreprocessorDirectivesConfigurator : IProjectConfigurator
    {
        private List<RequestedDirective> _requestedDirectives = new List<RequestedDirective>();
        private List<IManagedDirectiveComparer> _managedDirectiveComparers = new List<IManagedDirectiveComparer>();

        public void AddConfig(XDocument config)
        {
            var directives = config.Element("directives");
            if (directives == null) return;

            foreach (var element in directives.Elements("manage"))
                AddManagedDirectives(element);

            foreach (var element in directives.Elements("define"))
                AddRequestedDirective(element);
        }

        private void AddManagedDirectives(XElement element)
        {
            string value = element.Attribute("directive")?.Value;
            if (!string.IsNullOrWhiteSpace(value))
            {
                _managedDirectiveComparers.Add(new ManagedDirectivesEqualsComparer(value));
                return;
            }

            value = element.Attribute("pattern")?.Value;
            if (!string.IsNullOrEmpty(value))
                _managedDirectiveComparers.Add(new ManagedDirectivesPattenComparer(value));
        }

        private void AddRequestedDirective(XElement element)
        {
            string value = element.Attribute("directive")?.Value;
            if (string.IsNullOrEmpty(value)) return;

            var directive = new RequestedDirective(value);

            // add conditions
            var conditions = ConditionUtils.ParseConditions(element.Elements("conditions"));
            directive.Conditions.Add(conditions);

            // register directive
            _requestedDirectives.Add(directive);
        }

        private bool IsManagedDirective(string directive)
        {
            foreach (var comparer in _managedDirectiveComparers)
                if (comparer.IsMatched(directive))
                    return true;

            return false;
        }

        public void Apply()
        {
            //EditorApplication.LockReloadAssemblies();
            bool changed =
                ApplySafely(ApplyForSelectedBuildTargetGroup) ||
                ApplySafely(ApplyForAllBuildTargetGroups);
            //EditorApplication.UnlockReloadAssemblies();

            if (changed) AssetDatabase.SaveAssets();
        }

        private bool ApplySafely(Func<bool> action)
        {
            try
            {
                return action();
            }
            catch
            {
                return false;
            }
        }

        private bool ApplyForSelectedBuildTargetGroup() =>
            Apply(EditorUserBuildSettings.selectedBuildTargetGroup);

        private bool ApplyForAllBuildTargetGroups()
        {
            var groups = PlayerSettingsUtils.GetBuildTargetGroupsForScriptingDefineSymbols();
            if (groups.Count == 0) return false;

            bool changed = false;
            foreach (var buildTargetGroup in PlayerSettingsUtils.GetBuildTargetGroupsForScriptingDefineSymbols())
                changed |= Apply(buildTargetGroup);

            return changed;
        }

        private bool Apply(BuildTargetGroup buildTargetGroup)
        {
            // get current directives
            var currentDirectives = new PreprocessorDirectivesCollection(
                PlayerSettings.GetScriptingDefineSymbolsForGroup(buildTargetGroup));

            // remove managed directives
            var directives = new PreprocessorDirectivesCollection(
                currentDirectives.Where(p => !IsManagedDirective(p)));

            // add new directives
            directives.UnionWith(_requestedDirectives
                .Where(p => p.IsMatched())
                .Select(p => p.Name));

            // check changes
            if (directives.Count == currentDirectives.Count)
            {
                bool changed = false;
                foreach (var directive in directives)
                {
                    if (!currentDirectives.Contains(directive))
                    {
                        changed = true;
                        break;
                    }
                }

                if (!changed) return false;
            }

            // apply directives
            PlayerSettings.SetScriptingDefineSymbolsForGroup(buildTargetGroup, directives.ToString());
            //UnityEditor.Compilation.CompilationPipeline.RequestScriptCompilation();
            return true;
        }
    }
}
