﻿using System.Xml.Linq;

namespace Orion.ProjectConfig.Editor
{
    internal interface IProjectConfigurator
    {
        void AddConfig(XDocument config);
        void Apply();
    }
}
