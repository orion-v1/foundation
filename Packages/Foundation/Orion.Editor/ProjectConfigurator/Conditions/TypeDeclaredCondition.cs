﻿using System;
using System.Linq;

namespace Orion.ProjectConfig.Editor
{
    internal class TypeDeclaredCondition : ICondition
    {
        public const string AttributeTag = "requireClass";

        private readonly string _typeName;

        public TypeDeclaredCondition(string fullName)
        {
            _typeName = fullName;
        }

        public bool IsMatched()
        {
            return AppDomain.CurrentDomain.GetAssemblies()
                .Any(p => p.GetType(_typeName) != null);
        }
    }
}
