﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using UnityEngine;

namespace Orion.ProjectConfig.Editor
{
    internal static class ConditionUtils
    {
        public const string ConditionsTag = "conditions";
        public const string ConditionTag = "condition";


        public static IEnumerable<ICondition> ParseConditions(IEnumerable<XElement> elements)
        {
            if (elements == null) return null;

            return elements
                .Select(p => ParseConditions(p))
                .Where(p => p != null);
        }

        public static ICondition ParseConditions(XElement element)
        {
            if (element == null) return null;
            if (element.Name != ConditionsTag)
            {
                Debug.LogError($"The element tag is '{element.Name}' but '{ConditionsTag}' expected.");
                return null;
            }

            var conditions = new AndCondition();

            foreach (var conditionElement in element.Elements(ConditionTag))
            {
                var condition = ParseCondition(conditionElement);
                if (condition != null) conditions.Add(condition);
            }

            return conditions.Count > 0 ? conditions : null;
        }

        public static ICondition ParseCondition(XElement element)
        {
            if (element == null) return null;
            if (element.Name != ConditionTag)
            {
                Debug.LogError($"The element tag is '{element.Name}' but '{ConditionTag}' expected.");
                return null;
            }

            var attr = element.FirstAttribute;
            switch (attr.Name.LocalName)
            {
                case TypeDeclaredCondition.AttributeTag: return new TypeDeclaredCondition(attr.Value);
                case AsmDefCondition.AttributeTag: return new AsmDefCondition(attr.Value);
                case AssemblyCondition.AttributeTag: return new AssemblyCondition(attr.Value);

                default:
                    Debug.LogError($"Unknown condition attribute '{attr.Name}'.");
                    return null;
            }
        }
    }
}
