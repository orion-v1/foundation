﻿namespace Orion.ProjectConfig.Editor
{
    internal interface ICondition
    {
        bool IsMatched();
    }
}
