﻿using System.Text.RegularExpressions;
using UnityEditor;
using UnityEditor.Compilation;

namespace Orion.ProjectConfig.Editor
{
    internal class AssemblyCondition : ICondition
    {
        public const string AttributeTag = "requireAssembly";

        private readonly string _regex;

        public AssemblyCondition(string regex)
        {
            _regex = regex;
        }

        public bool IsMatched()
        {
            var asms = CompilationPipeline.GetAssemblies();
            foreach (var asm in asms)
                if (Regex.IsMatch(asm.name, _regex)) return true;

            return false;
        }
    }
}
