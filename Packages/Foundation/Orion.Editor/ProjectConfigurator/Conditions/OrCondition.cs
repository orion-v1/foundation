﻿using System.Collections.Generic;

namespace Orion.ProjectConfig.Editor
{
    internal class OrCondition : ICondition
    {
        private List<ICondition> _conditions = new List<ICondition>();
        
        public int Count => _conditions.Count;

        public void Add(ICondition condition)
        {
            if (condition != null) _conditions.Add(condition);
        }

        public void Add(IEnumerable<ICondition> conditions)
        {
            if (conditions != null) _conditions.AddRange(conditions);
        }

        public bool IsMatched()
        {
            if (_conditions.Count == 0) return true;

            foreach (var condition in _conditions)
                if (condition.IsMatched()) return true;

            return false;
        }
    }
}
