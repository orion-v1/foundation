﻿using System.Collections.Generic;

namespace Orion.ProjectConfig.Editor
{
    internal class AndCondition : ICondition
    {
        private List<ICondition> _conditions = new List<ICondition>();

        public int Count => _conditions.Count;

        public void Add(ICondition condition)
        {
            if (condition != null) _conditions.Add(condition);
        }

        public bool IsMatched()
        {
            if (_conditions.Count == 0) return true;

            foreach (var condition in _conditions)
                if (!condition.IsMatched()) return false;

            return true;
        }
    }
}
