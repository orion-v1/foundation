﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEditor;

namespace Orion.ProjectConfig.Editor
{
    internal class AsmDefCondition : ICondition
    {
        public const string AttributeTag = "requireAsmDef";

        private readonly string _regex;

        public AsmDefCondition(string regex)
        {
            _regex = regex;
        }

        public bool IsMatched()
        {
            foreach (var guid in AssetDatabase.FindAssets("t:asmdef"))
            {
                var path = AssetDatabase.GUIDToAssetPath(guid);
                if (Regex.IsMatch(path, _regex)) return true;
            }

            return false;
        }
    }
}
