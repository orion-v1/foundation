﻿using Orion;
using System.Reflection;
using System.Runtime.CompilerServices;

[assembly: AssemblyCompany(OrionInfo.CompanyName)]
[assembly: AssemblyProduct(OrionInfo.ProductName)]
[assembly: AssemblyTitle(OrionInfo.ProductTitle + " Foundation")]
[assembly: AssemblyDescription("Main module of the framework")]
//[assembly: AssemblyVersion("1.0.0.0")]

[assembly: InternalsVisibleTo("Orion.Editor")]
[assembly: InternalsVisibleTo("Orion.Tests")]
