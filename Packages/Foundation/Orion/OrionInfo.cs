﻿namespace Orion
{
    public static class OrionInfo
    {
        public const string CompanyName = "Orion Community";
        public const string ProductName = "Orion Framework";
        public const string ProductTitle = "Orion";
    }
}
