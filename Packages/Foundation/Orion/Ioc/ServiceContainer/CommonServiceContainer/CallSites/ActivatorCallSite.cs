﻿using System;

namespace Orion.Ioc
{
    internal class ActivatorCallSite : ICallSite
    {
        public readonly Type ImplementationType;

        public ActivatorCallSite(Type implementationType)
        {
            ImplementationType = implementationType;
        }

        public CallSiteKind Kind => CallSiteKind.Activator;

        public object Accept(ICallSiteVisitor visitor, ServiceProviderEngineScope scope) => visitor.VisitActivatorCallSite(this, scope);
    }
}
