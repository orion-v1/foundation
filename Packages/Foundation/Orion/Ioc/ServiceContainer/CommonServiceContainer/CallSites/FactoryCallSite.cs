﻿using System;

namespace Orion.Ioc
{
    internal class FactoryCallSite : ICallSite
    {
        public readonly Func<IServiceProvider, object> ImplementationFactory;

        public FactoryCallSite(Func<IServiceProvider, object> implementationFactory)
        {
            ImplementationFactory = implementationFactory;
        }

        public CallSiteKind Kind => CallSiteKind.Factory;

        public object Accept(ICallSiteVisitor visitor, ServiceProviderEngineScope scope) => visitor.VisitFactoryCallSite(this, scope);
    }
}
