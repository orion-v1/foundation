﻿namespace Orion.Ioc
{
    public abstract class ServiceNullableSingleton<T> where T : class
    {
        private static T _instance;

        public static T Instance => _instance ?? (_instance = AppHost.ServiceContainer.GetService<T>());
    }
}
