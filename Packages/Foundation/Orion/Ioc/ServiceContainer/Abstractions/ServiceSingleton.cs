﻿namespace Orion.Ioc
{
    public abstract class ServiceSingleton<T> where T : class
    {
        private static T _instance;

        public static T Instance => _instance ?? (_instance = AppHost.ServiceContainer.GetRequiredService<T>());
    }
}
