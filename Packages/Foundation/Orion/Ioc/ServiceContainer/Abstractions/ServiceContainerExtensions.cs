﻿namespace Orion.Ioc
{
    public static class ServiceContainerExtensions
    {
        public static bool ContainsAbstraction<T>(this IServiceContainer container)
            => container.ContainsAbstraction(typeof(T));
    }
}
