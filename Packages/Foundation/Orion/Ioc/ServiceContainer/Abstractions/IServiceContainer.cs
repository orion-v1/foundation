﻿using System;

namespace Orion.Ioc
{
    /// <summary>
    /// Describes a service container. 
    /// </summary>
    public interface IServiceContainer : IServiceProvider, IServiceScopeFactory, IDisposable
    {
        /// <summary>
        /// Gets the number of services (implementations) contained in the service container.
        /// </summary>
        int Count { get; }

        /// <summary>
        /// Gets whether the service container contains any service binded to specified abstraction type.
        /// </summary>
        /// <param name="type">The type of abstraction to check.</param>
        /// <returns>True if abstraction is assigned to any service; otherwise, false.</returns>
        bool ContainsAbstraction(Type type);

        /// <summary>
        /// Adds the specified service to the service container.
        /// </summary>
        /// <param name="serviceDescriptor">The descriptor of service to add.</param>
        void AddService(IServiceDescriptor serviceDescriptor);

        /// <summary>
        /// Removes the specified service from the service container.
        /// </summary>
        /// <param name="serviceDescriptor">The descriptor of service to remove.</param>
        /// <returns>true if service is successfully removed; otherwise, false.
        /// This method also returns false if service was not found in the service container.</returns>
        bool RemoveService(IServiceDescriptor serviceDescriptor);
    }
}
