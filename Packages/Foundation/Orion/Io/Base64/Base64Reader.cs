﻿using System;
using System.IO;
using System.Runtime.CompilerServices;

// Source: https://en.wikibooks.org/wiki/Algorithm_Implementation/Miscellaneous/Base64

namespace Orion.Io.Base64
{
    public class Base64Reader : IDisposable
    {
        private int _buffer;
        private int _bufferLength;
        private bool _leaveOpen;
        private bool _disposed;

        public Stream BaseStream { get; }


        public Base64Reader(Stream input, bool leaveOpen = false)
        {
            BaseStream = input;
            _leaveOpen = leaveOpen;
        }

        public void Dispose()
        {
            if (_disposed) return;

            _disposed = true;

            if (!_leaveOpen) BaseStream.Dispose();
        }

        public void Close() => Dispose();

        public int ReadByte()
        {
            AssertThisNotDisposed();

            if (_bufferLength < 8)
            {
                // read block of 4 ascii chars
                for (int i = 0; i < 4; i++)
                {
                    int mapIndex = BaseStream.ReadByte();
                    if (mapIndex < 0)
                    {
                        // end of stream reached
                        if (i == 0) return -1;

                        // unexpected end of stream
                        throw new EndOfStreamException("Unexpected end of stream.");
                    }

                    // ignore pad-symbols
                    if (mapIndex == Base64CharSet.PadCharacter) continue;

                    // check
                    byte decoded = Base64CharSet.DecodeLookup[mapIndex];
                    if (decoded >= Base64CharSet.MaxSize)
                        throw new FormatException("Non-valid character in base64");

                    // extract 6-bit of real data and save it to buffer
                    _buffer <<= 6;
                    _bufferLength += 6;
                    _buffer |= decoded;
                }
            }

            _bufferLength -= 8;
            return (_buffer >> _bufferLength) & 0xFF;
        }

        public int Read(byte[] buffer, int offset, int count)
        {
            int edgeIndex = offset + count;
            count = 0;
            for (int i = offset; i < edgeIndex; i++)
            {
                int data = ReadByte();
                if (data < 0) break;

                buffer[i] = (byte)data;
                count++;
            }

            return count;
        }


        //[Conditional("DEBUG")]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        void AssertThisNotDisposed()
        {
            if (_disposed)
                throw new ObjectDisposedException(nameof(Base64Reader));
        }
    }
}
