﻿using System;
using System.IO;
using System.Runtime.CompilerServices;

// Source: https://en.wikibooks.org/wiki/Algorithm_Implementation/Miscellaneous/Base64

namespace Orion.Io.Base64
{
    public class Base64Writer : IDisposable
    {
        private int _buffer;
        private int _bufferIndex;
        private bool _leaveOpen;
        private bool _disposed;
        private bool _flushed;

        public Stream BaseStream { get; }


        public Base64Writer(Stream output, bool leaveOpen = false)
        {
            BaseStream = output;
            _leaveOpen = leaveOpen;
        }

        public void Dispose()
        {
            if (_disposed) return;

            Flush();
            _disposed = true;

            if (!_leaveOpen) BaseStream.Dispose();
        }

        public void Close() => Dispose();

        public void Write(byte value)
        {
            AssertThisNotDisposed();

            // save to buffer and convert to big endian
            _flushed = false;
            _buffer |= value << (16 - _bufferIndex * 8);
            _bufferIndex++;

            // flush completed buffer
            if (_bufferIndex == 3) Flush();
        }

        public void Write(byte[] buffer, int offset, int count)
        {
            int edgeIndex = offset + count;
            for (int i = offset; i < edgeIndex; i++)
                Write(buffer[i]);
        }

        public void Flush()
        {
            if (_flushed) return;

            switch (_bufferIndex % 3)
            {
                case 0:
                    BaseStream.WriteByte((byte)Base64CharSet.EncodeLookup[(_buffer & 0x00FC0000) >> 18]);
                    BaseStream.WriteByte((byte)Base64CharSet.EncodeLookup[(_buffer & 0x0003F000) >> 12]);
                    BaseStream.WriteByte((byte)Base64CharSet.EncodeLookup[(_buffer & 0x00000FC0) >> 6]);
                    BaseStream.WriteByte((byte)Base64CharSet.EncodeLookup[(_buffer & 0x0000003F)]);
                    _bufferIndex = 0;
                    _buffer = 0;
                    break;

                case 1:
                    long pos = BaseStream.Position; // keep position to overwrite incomplete buffer
                    BaseStream.WriteByte((byte)Base64CharSet.EncodeLookup[(_buffer & 0x00FC0000) >> 18]);
                    BaseStream.WriteByte((byte)Base64CharSet.EncodeLookup[(_buffer & 0x0003F000) >> 12]);
                    BaseStream.WriteByte(Base64CharSet.PadCharacter);
                    BaseStream.WriteByte(Base64CharSet.PadCharacter);
                    BaseStream.Position = pos;
                    break;

                case 2:
                    pos = BaseStream.Position; // keep position to overwrite incomplete buffer
                    BaseStream.WriteByte((byte)Base64CharSet.EncodeLookup[(_buffer & 0x00FC0000) >> 18]);
                    BaseStream.WriteByte((byte)Base64CharSet.EncodeLookup[(_buffer & 0x0003F000) >> 12]);
                    BaseStream.WriteByte((byte)Base64CharSet.EncodeLookup[(_buffer & 0x00000FC0) >> 6]);
                    BaseStream.WriteByte(Base64CharSet.PadCharacter);
                    BaseStream.Position = pos;
                    break;
            }

            _flushed = true;
        }


        //[Conditional("DEBUG")]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        void AssertThisNotDisposed()
        {
            if (_disposed)
                throw new ObjectDisposedException(nameof(Base64Writer));
        }
    }
}
