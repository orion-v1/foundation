﻿using System;
using System.IO;
using System.Runtime.CompilerServices;

namespace Orion.Io.Base64
{
    public class Base64Stream : Stream
    {
        private readonly Stream _baseStream;
        private readonly Base64StreamMode _mode;
        private readonly Base64Reader _reader;
        private readonly Base64Writer _writer;
        private bool _disposed = false;


        public override bool CanRead => _mode == Base64StreamMode.Decode && _baseStream.CanRead;

        public override bool CanSeek => false;

        public override bool CanWrite => _mode == Base64StreamMode.Encode && _baseStream.CanWrite;

        public override long Length => throw new NotSupportedException();

        public override long Position
        {
            get => throw new NotSupportedException();
            set => throw new NotSupportedException();
        }


        public Base64Stream(Stream baseStream, Base64StreamMode mode, bool leaveOpen = false)
        {
            _baseStream = baseStream;
            _mode = mode;

            switch (mode)
            {
                case Base64StreamMode.Decode:
                    _reader = new Base64Reader(baseStream, leaveOpen);
                    break;

                case Base64StreamMode.Encode:
                    _writer = new Base64Writer(baseStream, leaveOpen);
                    break;

                default:
                    throw new NotImplementedException();
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _disposed = true;
                _reader?.Dispose();
                _writer?.Dispose();
            }

            base.Dispose(disposing);
        }

        public override int ReadByte()
        {
            AssertThisNotDisposed();
            AssertCanRead();

            return _reader.ReadByte();
        }

        public override void WriteByte(byte value)
        {
            AssertThisNotDisposed();
            AssertCanWrite();

            _writer.Write(value);
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            AssertThisNotDisposed();
            AssertCanRead();

            return _reader.Read(buffer, offset, count);
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            AssertThisNotDisposed();
            AssertCanWrite();

            _writer.Write(buffer, offset, count);
        }

        public override void Flush()
        {
            AssertThisNotDisposed();
            AssertCanWrite();

            _writer.Flush();
            _baseStream.Flush();
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            throw new NotSupportedException();
        }

        public override void SetLength(long value)
        {
            throw new NotSupportedException();
        }


        //[Conditional("DEBUG")]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        void AssertThisNotDisposed()
        {
            if (_disposed)
                throw new ObjectDisposedException(nameof(Base64Stream));
        }

        //[Conditional("DEBUG")]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        void AssertCanRead()
        {
            if (!CanRead)
                throw new NotSupportedException("The stream does not support reading.");
        }

        //[Conditional("DEBUG")]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        void AssertCanWrite()
        {
            if (!CanWrite)
                throw new NotSupportedException("The stream does not support writing.");
        }
    }
}
