﻿using System.Collections;
using System.Collections.Generic;

namespace Orion.ApplicationHost
{
    internal class AppHostLayersCollection : IEnumerable<AppHostLayer>
    {
        private List<AppHostLayer> _layers = new List<AppHostLayer>();
        private bool _sorted = true;

        public int Count => _layers.Count;


        public void Add(AppHostLayer layer)
        {
            if (_layers.Contains(layer)) return;

            _layers.Add(layer);
            _sorted = false;
        }

        public void Add(IEnumerable<AppHostLayer> layers)
        {
            foreach (var layer in layers) Add(layer);
        }

        public void Remove(AppHostLayer layer)
        {
            _layers.Remove(layer);
        }

        public void Sort()
        {
            if (_sorted) return;

            // insertion sort for partially sorted collection
            for (int i = 1; i < _layers.Count; i++)
            {
                var layer = _layers[i];
                int j = i - 1;
                while (j >= 0 && _layers[j] < layer)
                {
                    _layers[j + 1] = _layers[j];
                    j--;
                }

                j++;
                if (i != j) _layers[j] = layer;
            }

            _sorted = true;
        }

        public void Clear()
        {
            _layers.Clear();
            _sorted = true;
        }

        public IEnumerator<AppHostLayer> GetEnumerator()
        {
            Sort();
            return _layers.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}
