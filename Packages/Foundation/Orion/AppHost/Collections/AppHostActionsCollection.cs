﻿using System.Collections.Generic;

namespace Orion.ApplicationHost
{
    internal class AppHostActionsCollection
    {
        private Dictionary<AppHostLayer, List<IAppHostAction>> _actions = new Dictionary<AppHostLayer, List<IAppHostAction>>();

        public void Add(IAppHostAction action)
        {
            var layer = action.Layer;

            if (!_actions.TryGetValue(layer, out var list))
            {
                list = new List<IAppHostAction>();
                _actions.Add(layer, list);
            }

            list.Add(action);
        }

        public void Clear()
        {
            _actions.Clear();
        }

        public IEnumerable<IAppHostAction> GetActions(AppHostLayer layer)
        {
            _actions.TryGetValue(layer, out var list);
            return list;
        }
    }
}
