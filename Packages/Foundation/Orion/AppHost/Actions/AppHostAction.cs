﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Orion
{
    internal class AppHostAction : IAppHostAction
    {
        private Action<IAppHost> _action;

        public AppHostLayer Layer { get; }

        public AppHostQueue Queue { get; }

        public string ServiceName { get; }


        public AppHostAction(AppHostLayer layer, AppHostQueue queue, string serviceName, Action<IAppHost> action)
        {
            Layer = layer;
            Queue = queue;
            ServiceName = serviceName;
            _action = action ?? throw new ArgumentNullException(nameof(action));
        }

        public AppHostAction(AppHostLayer layer, AppHostQueue queue, Action<IAppHost> action) : this(layer, queue, null, action) { }

        public AppHostAction(AppHostLayer layer, string serviceName, Action<IAppHost> action) : this(layer, AppHostQueue.Main, serviceName, action) { }

        public AppHostAction(AppHostLayer layer, Action<IAppHost> action) : this(layer, AppHostQueue.Main, null, action) { }

        public Task Execute(IAppHost appHost, CancellationToken cancellationToken)
        {
            _action(appHost);
            return Task.CompletedTask;
        }

        public override string ToString() => GetActionDisplayName(_action);

        internal static string GetActionDisplayName<T>(T action) where T : Delegate
        {
            if (action == null) return "null";

            var m = action.Method.Name;
            var c = action.Method.DeclaringType.FullName;
            return $"{c}.{m}";
        }
    }


    internal class AppHostAction<T> : IAppHostAction
    {
        private Action<IAppHost, T> _action;
        private T _value;

        public AppHostLayer Layer { get; }

        public AppHostQueue Queue { get; }

        public string ServiceName { get; }


        public AppHostAction(AppHostLayer layer, AppHostQueue queue, string serviceName, Action<IAppHost, T> action, T value)
        {
            Layer = layer;
            Queue = queue;
            ServiceName = serviceName;
            _action = action ?? throw new ArgumentNullException(nameof(action));
            _value = value;
        }

        public AppHostAction(AppHostLayer layer, AppHostQueue queue, Action<IAppHost, T> action, T value) : this(layer, queue, null, action, value) { }

        public AppHostAction(AppHostLayer layer, string serviceName, Action<IAppHost, T> action, T value) : this(layer, AppHostQueue.Main, serviceName, action, value) { }

        public AppHostAction(AppHostLayer layer, Action<IAppHost, T> action, T value) : this(layer, AppHostQueue.Main, null, action, value) { }

        public Task Execute(IAppHost appHost, CancellationToken cancellationToken)
        {
            _action(appHost, _value);
            return Task.CompletedTask;
        }

        public override string ToString() => AppHostAction.GetActionDisplayName(_action);
    }


    public static class AppHostActionExtensions
    {
        public static IAppHostBuilder AddConfigureAction(
            this IAppHostBuilder appHostBuilder,
            AppHostLayer layer,
            AppHostQueue queue,
            Action<IAppHost> action)
        {
            return appHostBuilder.AddConfigureAction(new AppHostAction(layer, queue, action));
        }

        public static IAppHostBuilder AddConfigureAction(
            this IAppHostBuilder appHostBuilder,
            AppHostLayer layer,
            AppHostQueue queue,
            string serviceName,
            Action<IAppHost> action)
        {
            return appHostBuilder.AddConfigureAction(new AppHostAction(layer, queue, serviceName, action));
        }

        public static IAppHostBuilder AddConfigureAction(
            this IAppHostBuilder appHostBuilder,
            AppHostQueue queue,
            Action<IAppHost> action)
        {
            return appHostBuilder.AddConfigureAction(new AppHostAction(AppHostLayer.Default, queue, action));
        }

        public static IAppHostBuilder AddConfigureAction(
            this IAppHostBuilder appHostBuilder,
            AppHostQueue queue,
            string serviceName,
            Action<IAppHost> action)
        {
            return appHostBuilder.AddConfigureAction(new AppHostAction(AppHostLayer.Default, queue, serviceName, action));
        }


        public static IAppHostBuilder AddConfigureAction(
            this IAppHostBuilder appHostBuilder,
            AppHostLayer layer,
            Action<IAppHost> action)
        {
            return appHostBuilder.AddConfigureAction(new AppHostAction(layer, action));
        }

        public static IAppHostBuilder AddConfigureAction(
            this IAppHostBuilder appHostBuilder,
            AppHostLayer layer,
            string serviceName,
            Action<IAppHost> action)
        {
            return appHostBuilder.AddConfigureAction(new AppHostAction(layer, serviceName, action));
        }

        public static IAppHostBuilder AddConfigureAction(
            this IAppHostBuilder appHostBuilder,
            Action<IAppHost> action)
        {
            return appHostBuilder.AddConfigureAction(new AppHostAction(AppHostLayer.Default, action));
        }

        public static IAppHostBuilder AddConfigureAction(
            this IAppHostBuilder appHostBuilder,
            string serviceName,
            Action<IAppHost> action)
        {
            return appHostBuilder.AddConfigureAction(new AppHostAction(AppHostLayer.Default, serviceName, action));
        }


        public static IAppHostBuilder AddRunAction(
            this IAppHostBuilder appHostBuilder,
            AppHostLayer layer,
            AppHostQueue queue,
            Action<IAppHost> action)
        {
            return appHostBuilder.AddRunAction(new AppHostAction(layer, queue, action));
        }

        public static IAppHostBuilder AddRunAction(
            this IAppHostBuilder appHostBuilder,
            AppHostLayer layer,
            AppHostQueue queue,
            string serviceName,
            Action<IAppHost> action)
        {
            return appHostBuilder.AddRunAction(new AppHostAction(layer, queue, serviceName, action));
        }

        public static IAppHostBuilder AddRunAction(
            this IAppHostBuilder appHostBuilder,
            AppHostQueue queue,
            Action<IAppHost> action)
        {
            return appHostBuilder.AddRunAction(new AppHostAction(AppHostLayer.Default, queue, action));
        }

        public static IAppHostBuilder AddRunAction(
            this IAppHostBuilder appHostBuilder,
            AppHostQueue queue,
            string serviceName,
            Action<IAppHost> action)
        {
            return appHostBuilder.AddRunAction(new AppHostAction(AppHostLayer.Default, queue, serviceName, action));
        }


        public static IAppHostBuilder AddRunAction(
            this IAppHostBuilder appHostBuilder,
            AppHostLayer layer,
            Action<IAppHost> action)
        {
            return appHostBuilder.AddRunAction(new AppHostAction(layer, action));
        }

        public static IAppHostBuilder AddRunAction(
            this IAppHostBuilder appHostBuilder,
            AppHostLayer layer,
            string serviceName,
            Action<IAppHost> action)
        {
            return appHostBuilder.AddRunAction(new AppHostAction(layer, serviceName, action));
        }

        public static IAppHostBuilder AddRunAction(
            this IAppHostBuilder appHostBuilder,
            Action<IAppHost> action)
        {
            return appHostBuilder.AddRunAction(new AppHostAction(AppHostLayer.Default, action));
        }

        public static IAppHostBuilder AddRunAction(
            this IAppHostBuilder appHostBuilder,
            string serviceName,
            Action<IAppHost> action)
        {
            return appHostBuilder.AddRunAction(new AppHostAction(AppHostLayer.Default, serviceName, action));
        }
    }
}
