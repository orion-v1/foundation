﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Orion
{
    internal class AppHostActionAsync : IAppHostAction
    {
        private Func<IAppHost, CancellationToken, Task> _action;

        public AppHostLayer Layer { get; }

        public AppHostQueue Queue { get; }

        public string ServiceName { get; }


        public AppHostActionAsync(AppHostLayer layer, AppHostQueue queue, string serviceName, Func<IAppHost, CancellationToken, Task> action)
        {
            Layer = layer;
            Queue = queue;
            ServiceName = serviceName;
            _action = action ?? throw new ArgumentNullException(nameof(action));
        }

        public AppHostActionAsync(AppHostLayer layer, AppHostQueue queue, Func<IAppHost, CancellationToken, Task> action) : this(layer, queue, null, action) { }

        public AppHostActionAsync(AppHostLayer layer, string serviceName, Func<IAppHost, CancellationToken, Task> action) : this(layer, AppHostQueue.Main, serviceName, action) { }

        public AppHostActionAsync(AppHostLayer layer, Func<IAppHost, CancellationToken, Task> action) : this(layer, AppHostQueue.Main, null, action) { }

        public Task Execute(IAppHost appHost, CancellationToken cancellationToken)
            => _action(appHost, cancellationToken);

        public override string ToString() => AppHostAction.GetActionDisplayName(_action);
    }


    internal class AppHostActionAsync<T> : IAppHostAction
    {
        private Func<IAppHost, T, CancellationToken, Task> _action;
        private T _v;

        public AppHostLayer Layer { get; }

        public AppHostQueue Queue { get; }

        public string ServiceName { get; }


        public AppHostActionAsync(AppHostLayer layer, AppHostQueue queue, string serviceName, Func<IAppHost, T, CancellationToken, Task> action, T v)
        {
            Layer = layer;
            Queue = queue;
            ServiceName = serviceName;
            _action = action ?? throw new ArgumentNullException(nameof(action));
            _v = v;
        }

        public AppHostActionAsync(AppHostLayer layer, AppHostQueue queue, Func<IAppHost, T, CancellationToken, Task> action, T v) : this(layer, queue, null, action, v) { }

        public AppHostActionAsync(AppHostLayer layer, string serviceName, Func<IAppHost, T, CancellationToken, Task> action, T v) : this(layer, AppHostQueue.Main, serviceName, action, v) { }

        public AppHostActionAsync(AppHostLayer layer, Func<IAppHost, T, CancellationToken, Task> action, T v) : this(layer, AppHostQueue.Main, null, action, v) { }

        public Task Execute(IAppHost appHost, CancellationToken cancellationToken)
            => _action(appHost, _v, cancellationToken);

        public override string ToString() => AppHostAction.GetActionDisplayName(_action);
    }


    public static class AppHostActionAsyncExtensions
    {
        public static IAppHostBuilder AddConfigureAction(
            this IAppHostBuilder appHostBuilder,
            AppHostLayer layer,
            AppHostQueue queue,
            Func<IAppHost, CancellationToken, Task> action)
        {
            return appHostBuilder.AddConfigureAction(new AppHostActionAsync(layer, queue, action));
        }

        public static IAppHostBuilder AddConfigureAction(
            this IAppHostBuilder appHostBuilder,
            AppHostLayer layer,
            AppHostQueue queue,
            string serviceName,
            Func<IAppHost, CancellationToken, Task> action)
        {
            return appHostBuilder.AddConfigureAction(new AppHostActionAsync(layer, queue, serviceName, action));
        }

        public static IAppHostBuilder AddConfigureAction(
            this IAppHostBuilder appHostBuilder,
            AppHostQueue queue,
            Func<IAppHost, CancellationToken, Task> action)
        {
            return appHostBuilder.AddConfigureAction(new AppHostActionAsync(AppHostLayer.Default, queue, action));
        }

        public static IAppHostBuilder AddConfigureAction(
            this IAppHostBuilder appHostBuilder,
            AppHostQueue queue,
            string serviceName,
            Func<IAppHost, CancellationToken, Task> action)
        {
            return appHostBuilder.AddConfigureAction(new AppHostActionAsync(AppHostLayer.Default, queue, serviceName, action));
        }


        public static IAppHostBuilder AddConfigureAction(
            this IAppHostBuilder appHostBuilder,
            AppHostLayer layer,
            Func<IAppHost, CancellationToken, Task> action)
        {
            return appHostBuilder.AddConfigureAction(new AppHostActionAsync(layer, action));
        }

        public static IAppHostBuilder AddConfigureAction(
            this IAppHostBuilder appHostBuilder,
            AppHostLayer layer,
            string serviceName,
            Func<IAppHost, CancellationToken, Task> action)
        {
            return appHostBuilder.AddConfigureAction(new AppHostActionAsync(layer, serviceName, action));
        }

        public static IAppHostBuilder AddConfigureAction(
            this IAppHostBuilder appHostBuilder,
            Func<IAppHost, CancellationToken, Task> action)
        {
            return appHostBuilder.AddConfigureAction(new AppHostActionAsync(AppHostLayer.Default, action));
        }

        public static IAppHostBuilder AddConfigureAction(
            this IAppHostBuilder appHostBuilder,
            string serviceName,
            Func<IAppHost, CancellationToken, Task> action)
        {
            return appHostBuilder.AddConfigureAction(new AppHostActionAsync(AppHostLayer.Default, serviceName, action));
        }


        public static IAppHostBuilder AddRunAction(
            this IAppHostBuilder appHostBuilder,
            AppHostLayer layer,
            AppHostQueue queue,
            Func<IAppHost, CancellationToken, Task> action)
        {
            return appHostBuilder.AddRunAction(new AppHostActionAsync(layer, queue, action));
        }

        public static IAppHostBuilder AddRunAction(
            this IAppHostBuilder appHostBuilder,
            AppHostLayer layer,
            AppHostQueue queue,
            string serviceName,
            Func<IAppHost, CancellationToken, Task> action)
        {
            return appHostBuilder.AddRunAction(new AppHostActionAsync(layer, queue, serviceName, action));
        }

        public static IAppHostBuilder AddRunAction(
            this IAppHostBuilder appHostBuilder,
            AppHostQueue queue,
            Func<IAppHost, CancellationToken, Task> action)
        {
            return appHostBuilder.AddRunAction(new AppHostActionAsync(AppHostLayer.Default, queue, action));
        }

        public static IAppHostBuilder AddRunAction(
            this IAppHostBuilder appHostBuilder,
            AppHostQueue queue,
            string serviceName,
            Func<IAppHost, CancellationToken, Task> action)
        {
            return appHostBuilder.AddRunAction(new AppHostActionAsync(AppHostLayer.Default, queue, serviceName, action));
        }


        public static IAppHostBuilder AddRunAction(
            this IAppHostBuilder appHostBuilder,
            AppHostLayer layer,
            Func<IAppHost, CancellationToken, Task> action)
        {
            return appHostBuilder.AddRunAction(new AppHostActionAsync(layer, action));
        }

        public static IAppHostBuilder AddRunAction(
            this IAppHostBuilder appHostBuilder,
            AppHostLayer layer,
            string serviceName,
            Func<IAppHost, CancellationToken, Task> action)
        {
            return appHostBuilder.AddRunAction(new AppHostActionAsync(layer, serviceName, action));
        }

        public static IAppHostBuilder AddRunAction(
            this IAppHostBuilder appHostBuilder,
            Func<IAppHost, CancellationToken, Task> action)
        {
            return appHostBuilder.AddRunAction(new AppHostActionAsync(AppHostLayer.Default, action));
        }

        public static IAppHostBuilder AddRunAction(
            this IAppHostBuilder appHostBuilder,
            string serviceName,
            Func<IAppHost, CancellationToken, Task> action)
        {
            return appHostBuilder.AddRunAction(new AppHostActionAsync(AppHostLayer.Default, serviceName, action));
        }
    }
}
