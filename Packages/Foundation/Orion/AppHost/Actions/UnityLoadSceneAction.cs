﻿using System.Threading;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Orion
{
    internal class UnityLoadSceneAction : IAppHostAction
    {
        public AppHostLayer Layer => AppHostLayer.Application;

        public AppHostQueue Queue => AppHostQueue.Main;

        public string ServiceName { get; }

        public string SceneName { get; }


        public UnityLoadSceneAction(string sceneName)
        {
            SceneName = sceneName;
        }

        public UnityLoadSceneAction(string serviceName, string sceneName)
        {
            ServiceName = serviceName;
            SceneName = sceneName;
        }

        public async Task Execute(IAppHost appHost, CancellationToken cancellationToken)
        {
            var scene = SceneManager.GetSceneByName(SceneName);
            if (scene.isLoaded)
            {
                Debug.LogWarning($"Scene '{SceneName}' is already loaded.");
                return;
            }

            var operation = SceneManager.LoadSceneAsync(SceneName, LoadSceneMode.Additive);

            while (!operation.isDone)
            {
                cancellationToken.ThrowIfCancellationRequested();
                await Task.Yield();
            }
        }
    }


    public static class UnityLoadSceneActionExtensions
    {
        private static bool? _ableToLoadScenes;

        public static IAppHostBuilder LoadScene(this IAppHostBuilder appHostBuilder, string sceneName, bool force = false)
        {
            if (!_ableToLoadScenes.HasValue)
                _ableToLoadScenes = SceneManager.sceneCount == 1;

            if (force || _ableToLoadScenes.Value)
                appHostBuilder.AddConfigureAction(new UnityLoadSceneAction(sceneName));

            return appHostBuilder;
        }

        public static IAppHostBuilder LoadScene(this IAppHostBuilder appHostBuilder, string serviceName, string sceneName, bool force = false)
        {
            if (!_ableToLoadScenes.HasValue)
                _ableToLoadScenes = SceneManager.sceneCount == 1;

            if (force || _ableToLoadScenes.Value)
                appHostBuilder.AddConfigureAction(new UnityLoadSceneAction(serviceName, sceneName));

            return appHostBuilder;
        }
    }
}
