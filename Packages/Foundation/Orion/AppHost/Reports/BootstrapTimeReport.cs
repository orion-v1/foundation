﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

namespace Orion.ApplicationHost
{
    public class BootstrapTimeReport
    {
        private Stopwatch _totalTimer;
        private float _totalTimeOffset;
        private Dictionary<string, Stopwatch> _serviceTimers;

        public TimeSpan TotalTime { get; private set; }
        public IReadOnlyDictionary<string, TimeSpan> Services { get; private set; }

        internal void Start()
        {
            _totalTimer = new Stopwatch();
            _serviceTimers = new Dictionary<string, Stopwatch>();
            _totalTimeOffset = Time.realtimeSinceStartup;
            _totalTimer.Start();
        }

        internal void Stop()
        {
            _totalTimer.Stop();
            TotalTime = _totalTimer.Elapsed + TimeSpan.FromSeconds(_totalTimeOffset);

            if (_serviceTimers.Count > 0)
            {
                var services = new Dictionary<string, TimeSpan>(_serviceTimers.Count);
                foreach (var service in _serviceTimers)
                {
                    service.Value.Stop();
                    services.Add(service.Key, service.Value.Elapsed);
                }

                _serviceTimers.Clear();
                Services = services;
            }
        }

        internal Stopwatch GetServiceTimer(string serviceName)
        {
            if (string.IsNullOrEmpty(serviceName)) return null;

            if (!_serviceTimers.TryGetValue(serviceName, out var timer))
            {
                timer = new Stopwatch();
                _serviceTimers.Add(serviceName, timer);
            }

            return timer;
        }
    }
}
