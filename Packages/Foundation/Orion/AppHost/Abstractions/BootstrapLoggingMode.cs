﻿using System;

namespace Orion
{
    [Flags]
    public enum BootstrapLoggingMode
    {
        Disabled = 0,
        Summary = 1,
        InPlace = 2,
    }
}
