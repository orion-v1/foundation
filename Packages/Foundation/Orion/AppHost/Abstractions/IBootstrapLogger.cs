﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Orion
{
    public interface IBootstrapLogger
    {
        bool EnableAutostop { get; }
        bool IsRunning { get; }
        Dictionary<string, object> Reports { get; set; }

        void Start();
        void Stop(bool success, Exception exception = null);
        void Log(LogType logType, string message);
    }


    public static class BootstrapLoggerExtensions
    {
        public static void Log(this IBootstrapLogger logger, string message) =>
            logger.Log(LogType.Log, message);

        public static void LogWarning(this IBootstrapLogger logger, string message) =>
            logger.Log(LogType.Warning, message);

        public static void LogError(this IBootstrapLogger logger, string message) =>
            logger.Log(LogType.Error, message);

        public static void AddReport(this IBootstrapLogger logger, string reportId, object report)
        {
            logger.Reports ??= new Dictionary<string, object>();
            logger.Reports.Add(reportId, report);
        }

        public static void AddReport(this IBootstrapLogger logger, object report) =>
            AddReport(logger, report.GetType().FullName, report);

        public static object GetReport(this IBootstrapLogger logger, string reportId) =>
            logger.Reports != null && logger.Reports.TryGetValue(reportId, out var report)
                ? report
                : null;

        public static T GetReport<T>(this IBootstrapLogger logger, string reportId) =>
            (T)GetReport(logger, reportId);

        public static T GetReport<T>(this IBootstrapLogger logger)
        {
            if (logger.Reports == null) return default;

            var report = GetReport<T>(logger, typeof(T).FullName);
            if (report != null) return report;

            foreach (var r in logger.Reports)
                if (r is T typedReport)
                    return typedReport;

            return default;
        }
    }
}
