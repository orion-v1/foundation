﻿using System;

namespace Orion
{
    [Flags]
    public enum GameObjectOptions
    {
        None = 0,

        /// <summary>
        /// Keep objects always alive. OnDisable and OnDestroy will never be called.
        /// </summary>
        AlwaysAlive = 1,
    }
}
