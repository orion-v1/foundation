﻿using System.Threading;
using System.Threading.Tasks;

namespace Orion
{
    public interface IAppHostAction
    {
        AppHostLayer Layer { get; }
        AppHostQueue Queue { get; }
        string ServiceName { get; }

        Task Execute(IAppHost appHost, CancellationToken cancellationToken);
    }
}
