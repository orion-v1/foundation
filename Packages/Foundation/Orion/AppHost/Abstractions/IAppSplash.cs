﻿using System.Threading;
using System.Threading.Tasks;

namespace Orion
{
    public interface IAppSplash
    {
        Task ShowAsync(CancellationToken cancellationToken);
        Task HideAsync(CancellationToken cancellationToken);
    }
}
