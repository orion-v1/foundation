﻿using System.Collections;
using UnityEngine;

namespace Orion
{
    public interface IAppHostGameObject
    {
        Coroutine StartCoroutine(IEnumerator routine);
        void StopCoroutine(Coroutine coroutine);

        void AddChild(GameObject gameObject, GameObjectOptions options);
    }


    public static class AppHostGameObjectExtensions
    {
        public static T CreateChild<T>(
            this IAppHostGameObject obj,
            GameObjectOptions options = GameObjectOptions.None)
            where T : Component
        {
            var go = new GameObject(typeof(T).Name);
            var cmp = go.AddComponent<T>();
            obj.AddChild(go, options);
            return cmp;
        }

        public static T CreateChild<T>(
            this IAppHostGameObject obj,
            string name,
            GameObjectOptions options = GameObjectOptions.None)
            where T : Component
        {
            var go = new GameObject(name);
            var cmp = go.AddComponent<T>();
            obj.AddChild(go, options);
            return cmp;
        }
    }
}
