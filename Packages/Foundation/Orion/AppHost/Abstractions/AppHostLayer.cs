﻿namespace Orion
{
    /// <summary>
    /// Layers of th application host
    /// </summary>
    public enum AppHostLayer
    {
        /// <summary>
        /// The top layer of the application (default).
        /// </summary>
        Application = 0,

        /// <summary>
        /// The default layer for all common services.
        /// </summary>
        CommonServices = 10,

        /// <summary>
        /// The default layer for all modules of common services.
        /// </summary>
        CommonServiceModules = 20,

        // ........
        // reserved
        // ........

        /// <summary>
        /// The layer of runtime environment.
        /// </summary>
        Environment = 50,

        // ........
        // reserved
        // ........

        /// <summary>
        /// The core layer of the framework.
        /// </summary>
        Core = 1000,

        /// <summary>
        /// Default layer. It's similar to <see cref="CommonServices"/>.
        /// </summary>
        Default = CommonServices,
    }
}
