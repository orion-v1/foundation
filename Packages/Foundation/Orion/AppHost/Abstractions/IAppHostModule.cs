﻿namespace Orion
{
    public interface IAppHostModule
    {
        //string Id { get; }
        void AttachToAppHostBuilder(IAppHostBuilder appHostBuilder);
    }
}
