﻿using Orion.Ioc;
using Orion.Threading;
using System;
using System.Threading.Tasks;

namespace Orion
{
    public interface IAppHostBuilder
    {
        IServiceContainer ServiceContainer { get; }

        IAppHostBuilder SetServiceContainer(IServiceContainer serviceContainer);
        IAppHostBuilder SetApplicationSplash(IAppSplash splash);
        IAppHostBuilder SetBootstrapLogger(IBootstrapLogger logger);
        IAppHostBuilder AddConfigureAction(IAppHostAction action);
        IAppHostBuilder AddRunAction(IAppHostAction action);
        IAppHostBuilder AddModule(IAppHostModule module, AddModuleOptions options = AddModuleOptions.None);

        IAppHost Build();
    }


    public static class AppHostBuilderExtensions
    {
        #region Run

        public static Task RunAsync(this IAppHostBuilder appHostBuilder)
        {
            IAppHost appHost = appHostBuilder.Build();
            return appHost.RunAsync();
        }

        public static void Run(
            this IAppHostBuilder appHostBuilder,
            Action<IAppHost> launchAction = null)
        {
            IAppHost appHost = appHostBuilder.Build();
            var task = appHost.RunAsync();

            if (launchAction != null)
                task.ContinueInContextWith(_ => launchAction(appHost));

            task.ApplyUnityExceptionLogger();
        }

        //public static Task RunAsync(this IAppHostBuilder appHostBuilder, IAppLifecycleHandler appLifecycleHandler)
        //{
        //    appHostBuilder.AddAppLifecycleHandler(appLifecycleHandler);
        //    IAppHost appHost = appHostBuilder.Build();
        //    return appHost.RunAsync()
        //        .ApplyUnityExceptionLogger();
        //}

        #endregion
    }
}
