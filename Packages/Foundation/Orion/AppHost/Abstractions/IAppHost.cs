﻿using Orion.Ioc;
using Orion.Threading;
using System.Threading;
using System.Threading.Tasks;

namespace Orion
{
    public interface IAppHost
    {
        IServiceContainer ServiceContainer { get; }
        IAppHostGameObject GameObject { get; }
        IBootstrapLogger BootstrapLogger { get; }

        Task RunAsync(CancellationToken cancellationToken);
    }


    public static class AppHostExtensions
    {
        public static Task RunAsync(this IAppHost appHost)
            => appHost.RunAsync(UnityCancellationTokenSource.GlobalToken);
    }
}
