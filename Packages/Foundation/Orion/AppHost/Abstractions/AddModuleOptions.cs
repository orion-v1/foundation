﻿using System;

namespace Orion
{
    [Flags]
    public enum AddModuleOptions
    {
        None = 0,
        AllowMultiple = 1,
    }
}
