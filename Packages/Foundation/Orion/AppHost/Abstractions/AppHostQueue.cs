﻿namespace Orion
{
    public enum AppHostQueue
    {
        /// <summary>
        /// The main execution queue in layer
        /// </summary>
        Main = 0,

        /// <summary>
        /// The backward execution queue in layer
        /// </summary>
        Backward,
    }
}
