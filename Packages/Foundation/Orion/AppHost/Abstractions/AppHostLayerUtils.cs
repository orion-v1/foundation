﻿using System.Collections.Generic;

namespace Orion
{
    public static class AppHostLayerUtils
    {
        public static IEnumerable<AppHostLayer> ToEnumerable(AppHostLayer layer) => new[] { layer };
        
        public static IEnumerable<AppHostLayer> ToEnumerable(params AppHostLayer[] layers) => layers;
    }
}
