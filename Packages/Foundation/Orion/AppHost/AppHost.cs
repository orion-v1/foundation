﻿using Orion.ApplicationHost;
using Orion.Ioc;
using System;

namespace Orion
{
    public static class AppHost
    {
        private static IAppHost _instance = null;

        public static IAppHost Instance
        {
            get => _instance;
            internal set { _instance = value; }
        }

        public static IServiceContainer ServiceContainer => _instance.ServiceContainer;

        internal static void AttachAppHost(IAppHost appHost)
        {
            if (_instance == appHost) return;

            if (_instance != null && appHost != null)
                throw new InvalidOperationException("Another application host is already attached.");

            _instance = appHost;

            // assing IAppHost abstraction
            _instance.ServiceContainer.CreateService()
                .SetImplementationInstance(appHost)
                .BindTo<IAppHost>(ServiceBindingOptions.Exclusive)
                .AsSingleton();
        }

        internal static void DetachAppHost(IAppHost appHost)
        {
            if (_instance != appHost)
            {
                // TODO: Get logger
                UnityEngine.Debug.LogWarning("Failed to detach application host because it is not attached.");
                return;
            }

            _instance = null;
        }

        public static IAppHostBuilder CreateDefault()
        {
            return new CommonAppHostBuilder();
        }
    }
}
