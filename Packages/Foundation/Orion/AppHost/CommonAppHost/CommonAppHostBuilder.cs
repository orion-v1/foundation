﻿using Orion.Ioc;
using System;
using System.Collections.Generic;

namespace Orion.ApplicationHost
{
    internal class CommonAppHostBuilder : IAppHostBuilder
    {
        private IServiceContainer _serviceContainer;
        private IAppSplash _appSplash;
        private IBootstrapLogger _logger;
        private HashSet<IAppHostModule> _modules = new HashSet<IAppHostModule>();
        private AppHostLayersCollection _layers = new AppHostLayersCollection();
        private AppHostActionsCollection _mainConfigureActions = new AppHostActionsCollection();
        private AppHostActionsCollection _mainRunActions = new AppHostActionsCollection();
        private AppHostActionsCollection _backwardConfigureActions = new AppHostActionsCollection();
        private AppHostActionsCollection _backwardRunActions = new AppHostActionsCollection();

        public IServiceContainer ServiceContainer
        {
            get => _serviceContainer ?? (_serviceContainer = new ServiceContainer());
            set
            {
                if (ServiceContainer.Count > 0)
                    throw new InvalidOperationException("Failed to change a ServiceContainer implementation because current implementation is already in use. To avoid this issue you should call SetServiceContainer on top before any other methods of IAppHostBuilder.");

                _serviceContainer = value;
            }
        }


        public IAppHostBuilder SetServiceContainer(IServiceContainer serviceContainer)
        {
            ServiceContainer = serviceContainer;
            return this;
        }

        public IAppHostBuilder SetApplicationSplash(IAppSplash splash)
        {
            _appSplash = splash;
            return this;
        }

        public IAppHostBuilder SetBootstrapLogger(IBootstrapLogger logger)
        {
            _logger?.Stop(false, new OperationCanceledException("Logger canceled."));
            _logger = logger;
            if (_logger != null && !_logger.IsRunning) _logger.Start();

            return this;
        }

        private AppHostActionsCollection GetConfigureActions(AppHostQueue queue)
        {
            switch (queue)
            {
                case AppHostQueue.Main: return _mainConfigureActions;
                case AppHostQueue.Backward: return _backwardConfigureActions;
                default: throw new NotImplementedException($"Queue '{queue}' not supported.");
            }
        }

        public IAppHostBuilder AddConfigureAction(IAppHostAction action)
        {
            if (action != null)
            {
                _logger?.Log($"Add configure action {action}");
                GetConfigureActions(action.Queue).Add(action);
                _layers.Add(action.Layer);
            }
            return this;
        }

        private AppHostActionsCollection GetRunActions(AppHostQueue queue)
        {
            switch (queue)
            {
                case AppHostQueue.Main: return _mainRunActions;
                case AppHostQueue.Backward: return _backwardRunActions;
                default: throw new NotImplementedException($"Queue '{queue}' not supported.");
            }
        }

        public IAppHostBuilder AddRunAction(IAppHostAction action)
        {
            if (action != null)
            {
                _logger?.Log($"Add run action {action}");
                GetRunActions(action.Queue).Add(action);
                _layers.Add(action.Layer);
            }
            return this;
        }

        public IAppHostBuilder AddModule(IAppHostModule module, AddModuleOptions options = AddModuleOptions.None)
        {
            if (module == null) return this;

            if (!options.HasFlag(AddModuleOptions.AllowMultiple) && _modules.Contains(module))
                throw new InvalidOperationException($"Module '{module.GetType()}' already exists.");

            _logger?.Log($"Add module {module}");
            _modules.Add(module);
            module.AttachToAppHostBuilder(this);
            return this;
        }


        public IAppHost Build()
        {
            _modules.Clear();

            return new CommonAppHost(
                ServiceContainer,
                _appSplash,
                _logger,
                _layers,
                _mainConfigureActions,
                _mainRunActions,
                _backwardConfigureActions,
                _backwardRunActions);
        }
    }
}
