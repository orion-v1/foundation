﻿using UnityEngine;

namespace Orion.ApplicationHost
{
    internal class CommonAppHostGameObject : MonoBehaviour, IAppHostGameObject
    {
        private GameObject _alwaysAliveRootObject;

        public static CommonAppHostGameObject Create()
        {
            // create main object
            var go = new GameObject("Orion.AppHost");
            DontDestroyOnLoad(go);
            var hostObj = go.AddComponent<CommonAppHostGameObject>();

            // create AlwaysAlive root object
            go = new GameObject("Orion.AppHost.AlwaysAlive");
            go.hideFlags = HideFlags.DontSave; // HideFlags.DontSave breaks calling OnDisable, OnDestory in children
            DontDestroyOnLoad(go);
            hostObj._alwaysAliveRootObject = go;

            return hostObj;
        }

        public void AddChild(GameObject gameObject, GameObjectOptions options)
        {
            if (!gameObject) return;

            var root = (options & GameObjectOptions.AlwaysAlive) == 0
                ? this.gameObject
                : _alwaysAliveRootObject;

            gameObject.transform.SetParent(root.transform, false);
        }
    }
}
