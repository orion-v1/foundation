﻿using Orion.Ioc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Orion.ApplicationHost
{
    internal class CommonAppHost : IAppHost
    {
        private IAppSplash _appSplash;
        private AppHostLayersCollection _layers;
        private AppHostActionsCollection _mainConfigureActions;
        private AppHostActionsCollection _mainRunActions;
        private AppHostActionsCollection _backwardConfigureActions;
        private AppHostActionsCollection _backwardRunActions;
        private BootstrapTimeReport _timeReport;

        public IServiceContainer ServiceContainer { get; }

        public IAppHostGameObject GameObject { get; }

        public IBootstrapLogger BootstrapLogger { get; }


        public CommonAppHost(
            IServiceContainer serviceContainer,
            IAppSplash appSplash,
            IBootstrapLogger logger,
            AppHostLayersCollection layers,
            AppHostActionsCollection mainConfigureActions,
            AppHostActionsCollection mainRunActions,
            AppHostActionsCollection backwardConfigureActions,
            AppHostActionsCollection backwardRunActions)
        {
            ServiceContainer = serviceContainer;
            GameObject = CommonAppHostGameObject.Create();
            BootstrapLogger = logger;
            _appSplash = appSplash;
            _layers = layers;
            _mainConfigureActions = mainConfigureActions;
            _mainRunActions = mainRunActions;
            _backwardConfigureActions = backwardConfigureActions;
            _backwardRunActions = backwardRunActions;
        }


        public async Task RunAsync(CancellationToken cancellationToken)
        {
            try
            {
                await RunAsyncInternal(cancellationToken);
            }
            catch (Exception e)
            {
                if (_timeReport != null) _timeReport.Stop();
                BootstrapLogger?.Stop(false, e);
                throw;
            }

            if (BootstrapLogger != null)
            {
                BootstrapLogger.Log("= AppHost Running Complete");

                if (_timeReport != null) BootstrapLogger.AddReport(_timeReport);
                if (BootstrapLogger.EnableAutostop)
                    BootstrapLogger.Stop(true);
            }
        }

        public async Task RunAsyncInternal(CancellationToken cancellationToken)
        {
            // Do it first! Attach the instance to AppHost wrapper
            AppHost.AttachAppHost(this);
            BootstrapLogger?.Log("= AppHost Running Started");

            // start report
            _timeReport = new BootstrapTimeReport();
            _timeReport.Start();

            // show application splash
            if (_appSplash != null)
            {
                BootstrapLogger?.Log("== Show splash");
                await _appSplash.ShowAsync(cancellationToken);
            }

            //// Create and setup app host behaviour
            //_appHostBehaviour = new GameObject("HiveAppHost").AddComponent<CommonAppHostBehaviour>();
            //_appHostBehaviour.AppHost = this;
            //UnityEngine.Object.DontDestroyOnLoad(_appHostBehaviour.gameObject);

            //// Setup and run native apphost plugin (stage 1)
            //await RunAsyncImpl(cancellationToken);

            // process app host layers
            foreach (var layer in _layers)
            {
                // configure
                await ExecuteActionsAsync(_mainConfigureActions.GetActions(layer), cancellationToken);
                await ExecuteActionsAsync(_backwardConfigureActions.GetActions(layer)?.Reverse(), cancellationToken);

                // run
                await ExecuteActionsAsync(_mainRunActions.GetActions(layer), cancellationToken);
                await ExecuteActionsAsync(_backwardRunActions.GetActions(layer)?.Reverse(), cancellationToken);
            }

            // hide application splash
            if (_appSplash != null)
            {
                BootstrapLogger?.Log("== Hide splash");
                await _appSplash.HideAsync(cancellationToken);
            }

            // clear
            _appSplash = null;
            _layers.Clear();
            _mainConfigureActions.Clear();
            _mainRunActions.Clear();
            _backwardConfigureActions.Clear();
            _backwardRunActions.Clear();

            // TODO: implement ProcessAppLaunch
            // App lifecycle
            //ProcessAppLaunch();

            _timeReport.Stop();
        }

        private async Task ExecuteActionsAsync(IEnumerable<IAppHostAction> actions, CancellationToken cancellationToken)
        {
            if (actions == null) return;

            foreach (var action in actions)
            {
                BootstrapLogger?.Log(!string.IsNullOrEmpty(action.ServiceName)
                    ? $"== Execute action {action} [Layer: {action.Layer}, Queue: {action.Queue}, Service = {action.ServiceName}]"
                    : $"== Execute action {action} [Layer: {action.Layer}, Queue: {action.Queue}]");

                var timer = _timeReport.GetServiceTimer(action.ServiceName);
                timer?.Start();
                await action.Execute(this, cancellationToken);
                timer?.Stop();

                cancellationToken.ThrowIfCancellationRequested();
            }
        }
    }
}
