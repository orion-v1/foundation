﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using UnityEngine;

namespace Orion
{
    public class CommonBootstrapLogger : IBootstrapLogger
    {
        private const string Tag = "BootstrapLogger";
        private Stopwatch _stopwatch;
        private TimeSpan _stopwatchOffset;
        private StringBuilder _log;

        public BootstrapLoggingMode LoggingMode { get; }

        public bool EnableAutostop { get; }

        public bool IsRunning { get; private set; }

        public Dictionary<string, object> Reports { get; set; }


        public CommonBootstrapLogger(
            BootstrapLoggingMode loggingMode = BootstrapLoggingMode.Summary,
            bool enableAutostop = true)
        {
            LoggingMode = loggingMode;
            EnableAutostop = enableAutostop;
        }


        protected virtual void LoggerStarted() { }
        protected virtual void LoggerStopped() { }
        protected virtual void MessageLogged(string message, LogType logType) { }
        protected virtual void MessageReceived(string message, LogType logType, TimeSpan timestamp) { }


        public void Start()
        {
            if (IsRunning) return;
            IsRunning = true;

            _log = new StringBuilder(4096);
            _log.AppendLine(Tag);

            _stopwatch = new Stopwatch();
            Log(LogType.Log, "App started");

            _stopwatch.Start();
            _stopwatchOffset = TimeSpan.FromSeconds(Time.realtimeSinceStartup);
            Log(LogType.Log, "Log started");

            LoggerStarted();
        }

        public void Stop(bool success, Exception exception = null)
        {
            if (!IsRunning) return;

            _stopwatch.Stop();

            LogType logType;
            if (success)
            {
                Log(LogType.Log, "Done");
                logType = LogType.Log;
            }
            else
            {
                _log.AppendLine();
                _log.AppendLine(exception.ToString());
                logType = LogType.Error;
            }

            LogToConsole(_log.ToString(), logType);
            _log.Clear();
            IsRunning = false;

            LoggerStopped();
        }

        public void Log(LogType logType, string message)
        {
            if (!IsRunning) return;
            if (LoggingMode == BootstrapLoggingMode.Disabled) return;

            string lv;
            switch (logType)
            {
                case LogType.Log: lv = "INF"; break;
                case LogType.Warning: lv = "WRN"; break;
                default: lv = "ERR"; break;
            }

            var timestamp = _stopwatch.Elapsed + _stopwatchOffset;
            MessageReceived(message, logType, timestamp);

            var msg = $"[{lv} | {timestamp}]: {message}";

            if ((LoggingMode & BootstrapLoggingMode.InPlace) != 0)
                LogToConsole(Tag + ": " + msg, logType);

            if ((LoggingMode & BootstrapLoggingMode.Summary) != 0)
                _log.AppendLine(msg);
        }

        private void LogToConsole(string message, LogType logType)
        {
            if (logType != LogType.Log && logType != LogType.Warning)
                logType = LogType.Error;

            var t = Application.GetStackTraceLogType(logType);
            Application.SetStackTraceLogType(logType, StackTraceLogType.None);

            switch (logType)
            {
                case LogType.Log: UnityEngine.Debug.Log(message); break;
                case LogType.Warning: UnityEngine.Debug.LogWarning(message); break;
                default: UnityEngine.Debug.LogError(message); break;
            }

            Application.SetStackTraceLogType(logType, t);

            MessageLogged(message, logType);
        }
    }
}
