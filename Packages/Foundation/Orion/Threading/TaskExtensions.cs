﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;

// about Forget(): https://www.meziantou.net/fire-and-forget-a-task-in-dotnet.htm

namespace Orion.Threading
{
    public static class TaskExtensions
    {
        public static Task ContinueInContextWith(this Task task, Action<Task> continuationAction)
            => task.ContinueWith(continuationAction, TaskScheduler.FromCurrentSynchronizationContext());

        public static Task ApplyUnityExceptionLogger(this Task task, bool logIfCanceled = false)
        {
            return task.ContinueWith(t =>
            {
                //UnityEngine.Debug.Log($"Thread Id = {System.Threading.Thread.CurrentThread.ManagedThreadId}");

                if (t.IsFaulted) UnityEngine.Debug.LogException(t.Exception);
                else if (logIfCanceled && t.IsCanceled) UnityEngine.Debug.LogError("Task cancelled");

            }, TaskScheduler.FromCurrentSynchronizationContext());
        }

        #region Forget

        public static void Forget(this Task task)
        {
            if (!task.IsCompleted || task.IsFaulted)
                _ = ForgetAwaited(task);
        }

        public static void Forget(this Task task, ForgetOptions options)
        {
            if (!task.IsCompleted || task.IsFaulted)
                _ = ForgetAwaited(task, options);
        }

        private async static Task ForgetAwaited(Task task) =>
            await new NoThrowAwaiter(task);

        private async static Task ForgetAwaited(Task task, ForgetOptions options)
        {
            await new NoThrowAwaiter(task);
            LogTaskStatus(task, options);
        }

        [DebuggerHidden]
        private static void LogTaskStatus(Task task, ForgetOptions options)
        {
            //UnityEngine.Debug.Log($"Thread Id = {System.Threading.Thread.CurrentThread.ManagedThreadId}");
            if (task.IsFaulted)
            {
                if ((options & ForgetOptions.LogFaulted) != 0)
                    UnityEngine.Debug.LogException(task.Exception);
            }
            else if (task.IsCanceled)
            {
                if ((options & ForgetOptions.LogCanceled) != 0)
                {
                    // TODO: log stack
                    UnityEngine.Debug.LogError("Task canceled.");
                }
            }
            else if (task.IsCompleted)
            {
                if ((options & ForgetOptions.LogSucceeded) != 0)
                    UnityEngine.Debug.Log("Task successfully compleleted.");
            }
        }

        #endregion
    }
}
