﻿using System;

namespace Orion.Threading
{
    [Flags]
    public enum ForgetOptions
    {
        None = 0,
        LogSucceeded = 1,
        LogFaulted = 2,
        LogCanceled = 4,
    }
}
