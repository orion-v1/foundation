﻿using System;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace Orion.Threading
{
    // From https://github.com/dotnet/aspnetcore/blob/bae39d367cf8b92a9fb004fd9515bd3e2d0a46bf/src/SignalR/common/Http.Connections/src/Internal/TaskExtensions.cs
    internal readonly struct NoThrowAwaiter : ICriticalNotifyCompletion
    {
        private readonly Task _task;
        public NoThrowAwaiter(Task task) => _task = task;
        public NoThrowAwaiter GetAwaiter() => this;
        public bool IsCompleted => _task.IsCompleted;
        public void GetResult() { _ = _task.Exception; } // Observe exception
        public void OnCompleted(Action continuation) => _task.GetAwaiter().OnCompleted(continuation);
        public void UnsafeOnCompleted(Action continuation) => _task.GetAwaiter().UnsafeOnCompleted(continuation);
    }
}
