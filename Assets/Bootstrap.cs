﻿using Orion;
using SimplePlugin;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;

namespace GameProject
{
    public class Bootstrap : MonoBehaviour
    {
        private void Awake()
        {
            AppHost.CreateDefault()
                .SetBootstrapLogger(BootstrapLogger.Current)
                .SetApplicationSplash(new AppSplash())
                //.AddConfigureAction(Configure)
                .AddConfigureAction(ConfigureAsync)
                .AddRunAction(RunAsync)
                //.AddSimplePlugin()
                .AddSimpleModule()
                .AddConfigureAction(AppHostLayer.Application, AppInitializeAsync)
                .LoadScene("Test1")
                .LoadScene("Test2")
                .AddRunAction(AppHostLayer.Application, AppLaunch)
                .Run();
        }

        private async Task ConfigureAsync(IAppHost appHost, CancellationToken cancellationToken)
        {
            Debug.Log("-> ConfigureAsync");
            await Task.Delay(1000, cancellationToken);
            Debug.Log("<- ConfigureAsync");
        }

        private async Task RunAsync(IAppHost appHost, CancellationToken cancellationToken)
        {
            Debug.Log("-> RunAsync");
            await Task.Delay(1000, cancellationToken);
            Debug.Log("<- RunAsync");
        }

        private async Task AppInitializeAsync(IAppHost appHost, CancellationToken cancellationToken)
        {
            Debug.Log("-> AppInitializeAsync");
            await Task.Delay(1000, cancellationToken);
            //throw new Exception("SDASD");
            Debug.Log("<- AppInitializeAsync");
        }

        private void AppInitialize(IAppHost appHost)
        {
            Debug.Log("AppInitialize");
        }

        private void AppLaunch(IAppHost appHost)
        {
            Debug.Log("OnLaunch");

            //var st = Environment.StackTrace;
            //var st = UnityEngine.StackTraceUtility.ExtractStackTrace();
            //int idx = st.IndexOf('\n');
            //st = st.Substring(idx + 1);
            //Debug.LogWarning(st);
        }
    }


    public class AppSplash : IAppSplash
    {
        public Task ShowAsync(CancellationToken cancellationToken)
        {
            Debug.Log("-> Show SPLASH");
            return Task.CompletedTask;
        }

        public Task HideAsync(CancellationToken cancellationToken)
        {
            Debug.Log("-> Hide SPLASH");
            return Task.CompletedTask;
        }
    }
}
