﻿using Orion;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;

namespace SimplePlugin
{
    internal class SimpleModule : IAppHostModule
    {
        public void AttachToAppHostBuilder(IAppHostBuilder appHostBuilder)
        {
            appHostBuilder.AddConfigureAction("SimpleSvc", ConfigureAsync);
            appHostBuilder.AddRunAction("SimpleSvc", RunAsync);

            appHostBuilder.AddConfigureAction(AppHostQueue.Backward, "SimpleSvc", ConfigureBackward);
            appHostBuilder.AddRunAction(AppHostQueue.Backward, "SimpleSvc", RunBackward);
        }

        private async Task ConfigureAsync(IAppHost appHost, CancellationToken cancellationToken)
        {
            const AppHostLayer layer = AppHostLayer.Default;

            Debug.Log($"-> ConfigureAsync [Layer = {layer}]");
            await Task.Delay(1000, cancellationToken);
            Debug.Log($"<- ConfigureAsync [Layer = {layer}]");
        }

        private async Task RunAsync(IAppHost appHost, CancellationToken cancellationToken)
        {
            const AppHostLayer layer = AppHostLayer.Default;

            Debug.Log($"-> RunAsync [Layer = {layer}]");
            await Task.Delay(1000, cancellationToken);
            Debug.Log($"<- RunAsync [Layer = {layer}]");
        }

        private void ConfigureBackward(IAppHost appHost)
        {
            Debug.Log("-> ConfigureBackward");
        }

        private void RunBackward(IAppHost appHost)
        {
            Debug.Log("-> RunBackward");
        }
    }

    public static class PluginExtensions
    {
        public static IAppHostBuilder AddSimpleModule(this IAppHostBuilder appHostBuilder)
            => appHostBuilder.AddModule(new SimpleModule());
    }
}