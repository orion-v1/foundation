﻿using Orion;
using UnityEngine;

namespace GameProject
{
    public class BootstrapLogger : CommonBootstrapLogger
    {
        #region Instancing

        private static bool _currentInited;

        public static BootstrapLogger Current { get; private set; }

        private static BootstrapLogger GetInstanceOrNull()
        {
            if (Current != null) return Current;
            if (_currentInited) return Current; // to avoid calling this method multiple times
            _currentInited = true;

            var reportingMode = BootstrapLoggingMode.Summary;
            if (reportingMode != BootstrapLoggingMode.Disabled)
            {
                Current = new BootstrapLogger(reportingMode);
                Current.Start();
            }

            return Current;
        }

        private BootstrapLogger(BootstrapLoggingMode reportingMode) : base(reportingMode) { }

        #endregion

        #region Built-in triggers

        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.SubsystemRegistration)]
        private static void UnitySubsystemRegistration()
            => GetInstanceOrNull()?.Log("Unity SubsystemRegistration");

        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSplashScreen)]
        private static void UnityBeforeSplashScreen()
            => GetInstanceOrNull()?.Log("Unity BeforeSplashScreen");

        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.AfterAssembliesLoaded)]
        private static void UnityAfterAssembliesLoaded()
            => GetInstanceOrNull()?.Log("Unity AfterAssembliesLoaded");

        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
        private static void UnityBeforeSceneLoad()
            => GetInstanceOrNull()?.Log("Unity BeforeSceneLoad");

        #endregion
    }
}
